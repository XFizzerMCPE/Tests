<?php
namespace XFizzer\Events;

use XFizzer\Loader;
use pocketmine\event\Listener;
use pocketmine\Player;
use pocketmine\item\Item;
use pocketmine\math\Vector3;
use pocketmine\entity\Effect;
use pocketmine\level\particle\FloatingTextParticle;
use pocketmine\event\player\
{
	PlayerInteractEvent, PlayerMoveEvent, PlayerRespawnEvent, PlayerJoinEvent, PlayerQuitEvent
};

class EventListener implements Listener
{

		public function __construct(Loader $plugin)
		{
			$this->plugin = $plugin;
			$this->text = new FloatingTextParticle(new Vector3(-73.5, 45.7, 46.5), "", "welp");
		}
		
		public function onJoin(PlayerJoinEvent $event)
		{
			$player = $event->getPlayer();
			$this->onText($player);
			$this->onTextUpdate($player);
			$player->addEffect(Effect::getEffect(16)->setDuration(999999)->setAmplifier(2)->setVisible(false));
			$player->addEffect(Effect::getEffect(1)->setDuration(999999)->setAmplifier(2)->setVisible(false));
		}
		
		public function onLeave(PlayerQuitEvent $event)
		{
			$player = $event->getPlayer();
			$this->onTextUpdate($player);
		}
		
		public function onText($player)
		{
			$level = $this->plugin->getServer()->getLevelByName("spawn2");
			$text = new FloatingTextParticle(new Vector3(-73.5, 46, 46.5), "", "Welcome to OmegaOP");
			$text1 = new FloatingTextParticle(new Vector3(-73.5, 45.4, 46.5), "", "blah blah");
			$text2 = new FloatingTextParticle(new Vector3(-43.5, 46, 79.5), "", "Mines");
			$level->addParticle($text, [$player]);
			$level->addParticle($text1, [$player]);
			$level->addParticle($text2, [$player]);
		}
		
		public function onTextUpdate($player)
		{
			$level = $this->plugin->getServer()->getLevelByName("spawn2");
			$online = count($this->plugin->getServer()->getOnlinePlayers());
			$this->text->setTitle("There is $online players online");
			if ($level) $level->addParticle($this->text);
		}
		
		public function onRespawn(PlayerRespawnEvent $event)
		{
			$player = $event->getPlayer();
			$player->addEffect(Effect::getEffect(16)->setDuration(999999)->setAmplifier(2)->setVisible(false));
			$player->addEffect(Effect::getEffect(1)->setDuration(999999)->setAmplifier(2)->setVisible(false));
		}
}